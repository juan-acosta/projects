<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}

    /**
     * Authenticates the password.
     *
     * @param $attribute
     * @param $params
     * @throws CException
     */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->authenticate();

            switch($this->_identity->errorCode)
            {
                case UserIdentity::ERROR_NONE:
                    $duration = $this->rememberMe ? Yii::app()->getModule('backend')->rememberMeTime : Yii::app()->getModule('backend')->defaultTime ;
                    Yii::app()->user->login($this->_identity, $duration);
                    break;
                case UserIdentity::ERROR_EMAIL_INVALID:
                    $this->addError("username","Usuario incorrecto.");
                    break;
                case UserIdentity::ERROR_STATUS_NOTACTIV:
                    $this->addError("status_id","La cuenta no está activa.");
                    break;
                case UserIdentity::ERROR_STATUS_BAN:
                    $this->addError("status_id","La cuenta está bloqueada.");
                    break;
                case UserIdentity::ERROR_PASSWORD_INVALID:
                    $this->addError("password","Clave incorrecta.");
                    break;
                case UserIdentity::ERROR_EMAIL_PASSWD_INVALID:
                    $this->addError("username","Usuario is incorrecto, o");
                    $this->addError("password","Clave is incorrecta.");
                    break;
            }
        }
	}


}
