<?php

/**
 * Description for DefaultController
 *
 * @var $this DefaultController
 * @package FrontendModule
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }
}