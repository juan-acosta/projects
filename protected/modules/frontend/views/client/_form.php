<?php
/** @var $this ClientController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'client-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('ruc')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'ruc', ['class'=>'']); ?>
        </div>
        <div class="four wide field">
            <?= $form->textField($model,'ruc',['class'=>'', ]); ?>
            <?= $form->error($model,'ruc',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('name_contributor')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'name_contributor', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'name_contributor',['class'=>'', ]); ?>
            <?= $form->error($model,'name_contributor',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('tradename')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'tradename', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'tradename',['class'=>'', ]); ?>
            <?= $form->error($model,'tradename',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('address_tax_office')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'address_tax_office', ['class'=>'']); ?>
        </div>
        <div class="eight wide field">
            <?= $form->textArea($model,'address_tax_office',['class'=>'', 'cols'=>3 ]); ?>
            <?= $form->error($model,'address_tax_office',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('actividad_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'actividad_id', ['class'=>'']); ?>
        </div>
        <div class="ten wide field">
            <?= $form->dropDownList($model, 'actividad_id', Client::itemAlias('actividad'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'actividad_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/frontend/client'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
