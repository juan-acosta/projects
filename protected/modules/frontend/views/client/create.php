<?php
/** @var $this ClientController */
/** @see frontend/client/create [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'Clients';
$this->headTitle = 'Crear Client';

$this->breadcrumbs = [
    'Clients' => ['index'],
    'Crear',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Completar el formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model] ); ?>
    </div>
</div>