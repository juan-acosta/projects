<?php
/** @var $this ClientController */
/** @see frontend/client/update [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'Clients';
$this->headTitle = 'Actualizar el/la Client ID#'.$model->id;

$this->breadcrumbs = [
    'Clients' => ['index'],
    'Actualizar',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Actualizar los datos del formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model]); ?>
    </div>
</div>