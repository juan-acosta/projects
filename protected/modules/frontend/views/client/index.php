<?php
/** @var $this ClientController */
/** @see frontend/client/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Clients';
$this->headTitle = 'Administrar Clients';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/frontend/client/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'Clients' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'client-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                'name'=>'ruc',
                //'header'=>'ruc',
                //'value'=>'ruc',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'name_contributor',
                //'header'=>'name_contributor',
                //'value'=>'name_contributor',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'tradename',
                //'header'=>'tradename',
                //'value'=>'tradename',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'address_tax_office',
                //'header'=>'address_tax_office',
                'value'=>'nl2br($data->address_tax_office)',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],

            /*
            'address_tax_office',
            'actividad_id',
            'created_at',
            'created_by',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/frontend/client/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/frontend/client/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
