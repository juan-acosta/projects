<?php
/** @var $this DefaultController */
/** @see default/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */


$this->pagetitle = 'Default ';
$this->headTitle = 'Default Index';

//$this->btnActions[] = [];

$this->breadcrumbs = [
    'Default' => ['index'],
    'Administrar',
];

?>
<h1><?= $this->uniqueId . '/' . $this->action->id; ?></h1>

<p>
    This is the view content for action "<?=  $this->action->id; ?>".
    The action belongs to the controller "<?= get_class($this); ?>"
    in the "<?= $this->module->id; ?>" module.
</p>
<p>
    You may customize this page by editing <tt><?= __FILE__; ?></tt>
</p>