<?php
/** @var $this ContactController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'contact-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('client_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'client_id', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->dropDownList($model, 'client_id', CHtml::listData(Client::model()->findAll(), 'id' ,'ruc_name'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'client_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('first_name')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
                <?= $form->labelEx($model, 'first_name', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'first_name',['class'=>'', ]); ?>
                <?= $form->error($model,'first_name',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('last_name')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
                <?= $form->labelEx($model, 'last_name', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'last_name',['class'=>'', ]); ?>
                <?= $form->error($model,'last_name',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('puesto_str')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
                <?= $form->labelEx($model, 'puesto_str', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'puesto_str',['class'=>'', ]); ?>
                <?= $form->error($model,'puesto_str',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('phone_oficina')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
                <?= $form->labelEx($model, 'phone_oficina', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'phone_oficina',['class'=>'', ]); ?>
                <?= $form->error($model,'phone_oficina',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('phone_mobile')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
                <?= $form->labelEx($model, 'phone_mobile', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'phone_mobile',['class'=>'', ]); ?>
                <?= $form->error($model,'phone_mobile',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/frontend/contact'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
