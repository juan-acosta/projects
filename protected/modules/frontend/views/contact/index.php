<?php
/** @var $this ContactController */
/** @see frontend/contact/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Contacts';
$this->headTitle = 'Administrar Contacts';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/frontend/contact/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'Contacts' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'contact-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                'name'=>'client_id',
                //'header'=>'client_id',
                'value'=>function($row,$data){
                    return $row->client->ruc_name;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'first_name',
                //'header'=>'first_name',
                //'value'=>'first_name',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'last_name',
                //'header'=>'last_name',
                //'value'=>'last_name',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'puesto_str',
                //'header'=>'puesto_str',
                //'value'=>'puesto_str',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'phone_oficina',
                //'header'=>'phone_oficina',
                //'value'=>'phone_oficina',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'phone_mobile',
                //'header'=>'phone_mobile',
                //'value'=>'phone_mobile',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            /*
            'phone_oficina',
            'phone_mobile',
            'created_at',
            'created_by',
            'client_id',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/frontend/contact/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'ui compact teal icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/frontend/contact/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
