<?php

/**
 * Model class for table contact.
 *
 * The columns in table:
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $puesto_str
 * @property string $phone_oficina
 * @property string $phone_mobile
 * @property string $created_at
 * @property integer $created_by
 * @property integer $client_id
 *
 * The model relations:
 * @property Client $client
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Contact extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['first_name, last_name, puesto_str, phone_oficina', 'required'],
            ['created_by, client_id', 'numerical', 'integerOnly'=>true],
            ['first_name, last_name', 'length', 'max'=>128],
            ['puesto_str', 'length', 'max'=>32],
            ['phone_oficina, phone_mobile', 'length', 'max'=>15],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, first_name, last_name, puesto_str, phone_oficina, phone_mobile, created_at, created_by, client_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'client' => [self::BELONGS_TO, 'Client', 'client_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Nombres',
            'last_name' => 'Apellidos',
            'puesto_str' => 'Puesto',
            'phone_oficina' => 'Telefono oficina',
            'phone_mobile' => 'Telefono celular',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'client_id' => 'Cliente',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('first_name',$this->first_name,true);
        $criteria->compare('last_name',$this->last_name,true);
        $criteria->compare('puesto_str',$this->puesto_str,true);
        $criteria->compare('phone_oficina',$this->phone_oficina,true);
        $criteria->compare('phone_mobile',$this->phone_mobile,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('client_id',$this->client_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Contact the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

}
