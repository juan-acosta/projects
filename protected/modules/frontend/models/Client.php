<?php

/**
 * Model class for table client.
 *
 * The columns in table:
 * @property integer $id
 * @property string $name_contributor
 * @property string $tradename
 * @property string $ruc
 * @property string $address_tax_office
 * @property integer $actividad_id
 * @property string $created_at
 * @property integer $created_by
 *
 * The model relations:
 * @property Contact[] $contacts
 * @property Project[] $projects
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Client extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;
    public $ruc_name;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'client';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name_contributor, ruc, address_tax_office', 'required'],
            ['actividad_id, created_by', 'numerical', 'integerOnly'=>true],
            ['name_contributor, tradename', 'length', 'max'=>250],
            ['ruc', 'length', 'max'=>20],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, name_contributor, tradename, ruc, address_tax_office, actividad_id, created_at, created_by', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'contacts' => [self::HAS_MANY, 'Contact', 'client_id'],
            'projects' => [self::HAS_MANY, 'Project', 'client_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_contributor' => 'Razon Social',
            'tradename' => 'Nombre comercial',
            'ruc' => 'RUC',
            'address_tax_office' => 'Domicilio fiscal',
            'actividad_id' => 'Actividad economica',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('name_contributor',$this->name_contributor,true);
        $criteria->compare('tradename',$this->tradename,true);
        $criteria->compare('ruc',$this->ruc,true);
        $criteria->compare('address_tax_office',$this->address_tax_office,true);
        $criteria->compare('actividad_id',$this->actividad_id);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Client the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);
        $this->ruc_name = $this->ruc . ' - ' .$this->name_contributor;

        return parent::afterFind();
    }

    /**
     * Alias array
     *
     * @param string $type
     * @param null $code
     * @return bool|mixed
     */
    public static function itemAlias($type, $code=NULL)
    {
        $_items = [
            'actividad' => [
                1 => 'Agricultura, agroindustria y ganadería.',
                2 => 'Almacenamiento y manipulación de carga.',
                3 => 'Bancos, financieras.',
                4 => 'Bebidas.',
                5 => 'Comercio de combustible y gas.',
                6 => 'Construcción y materiales de construcción.',
                7 => 'Educación.',
                8 => 'Elaboración de productos alimenticios.',
                9 => 'Electricidad - Agua.',
                10 => 'Grandes almacenes y cadenas de tiendas minoristas.',
                11 => 'Industria del papel, cartón, envases y actividades de edición e impresión ',
                12 => 'Industria farmaceútica y cosmética',
                13 => 'Industria química',
                14 => 'Inmobiliarias',
                15 => 'Marketing, Publicidad e Investigación',
                16 => 'Medios de comunicación (TV, radio, prensa, revistas)',
                17 => 'Minería',
                19 => 'Otros',
                20 => 'Pesca',
                21 => 'Petróleo',
                22=> 'Productos metal, hierro, acero y maquinaria',
                23 => 'Salud',
                24 => 'Seguridad, limpieza y servicios generales',
                25 => 'Seguros, AFP',
                26 => 'Servicio de transporte',
                27 => 'Servicios de Consultoría, Auditoría',
                28 => 'Servicios de sistemas, equipos de tecnología y comunicaciones',
                29 => 'Servicios Profesionales',
                30 => 'Telecomunicaciones',
                31 => 'Textiles y confecciones',
                32 => 'Turismo, hoteles, restaurantes y entretenimiento',
                33 => 'Vehículos y accesorios',
                34 => 'Venta al por mayor',
                35 => 'Venta al por menor',

            ],

        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }


    /**
     *
     * Validacion antes de eliminar
     *
     * @return bool|void
     * @throws CDbException
     */
    protected function beforeDelete()
    {

        // existe contactos?
        if( count($this->contacts) > 0 ){
            throw new CDbException('Existe dependencias con Contact');
        }

        return parent::beforeDelete();
    }

}
