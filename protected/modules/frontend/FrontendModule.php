<?php

/**
 * Description for FrontendModule
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class FrontendModule extends CWebModule
{
    public function init()
    {
        // import the module-level models and components
        $this->setImport([
            'frontend.models.*',
            'backend.models.*',
            'frontend.components.*',
        ]);
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }
}
