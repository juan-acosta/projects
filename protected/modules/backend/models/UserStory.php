<?php

/**
 * Model class for table user_story.
 *
 * The columns in table:
 * @property integer $id
 * @property string $storie
 * @property integer $points
 * @property integer $developer_id
 * @property string $compromiso_date
 * @property string $inicio_date
 * @property integer $hours_estimated
 * @property integer $hours_effective
 * @property integer $percent_finish
 * @property string $created_at
 * @property integer $created_by
 * @property integer $epic_id
 *
 * The model relations:
 * @property Task[] $tasks
 * @property Developer $developer
 * @property Epic $epic
 * @property SprintUs[] $sprintUses
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class UserStory extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_story';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['storie, developer_id, compromiso_date, inicio_date', 'required'],
            ['points, developer_id, hours_estimated, hours_effective, percent_finish, created_by, epic_id', 'numerical', 'integerOnly'=>true],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'tasks' => [self::HAS_MANY, 'Task', 'user_story_id'],
            'developer' => [self::BELONGS_TO, 'Developer', 'developer_id'],
            'epic' => [self::BELONGS_TO, 'Epic', 'epic_id'],
            'sprintUses' => [self::HAS_MANY, 'SprintUs', 'us_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storie' => 'Historia Usuario',
            'points' => 'Puntos de Historia',
            'developer_id' => 'Developer',
            'compromiso_date' => 'Fecha de compromiso',
            'inicio_date' => 'Fecha de Inicio',
            'hours_estimated' => 'Horas Estimadas',
            'hours_effective' => 'Horas Efectivas',
            'percent_finish' => 'Porcentaje Finalizar',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'epic_id' => 'Epic',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('storie',$this->storie,true);
        $criteria->compare('points',$this->points);
        $criteria->compare('developer_id',$this->developer_id);
        $criteria->compare('compromiso_date',$this->compromiso_date,true);
        $criteria->compare('inicio_date',$this->inicio_date,true);
        $criteria->compare('hours_estimated',$this->hours_estimated);
        $criteria->compare('hours_effective',$this->hours_effective);
        $criteria->compare('percent_finish',$this->percent_finish);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('epic_id',$this->epic_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return UserStory the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

    /**
     * Alias array
     *
     * @param string $type
     * @param null $code
     * @return bool|mixed
     */
    public static function itemAlias($type, $code=NULL)
    {
        $_items = [
            'points' => [
                1 => '1 point',
                2 => '2 points',
                3 => '3 points',
                5 => '5 points',
                8 => '8 points',
                13 => '13 points',
                20 => '20 points',
                30 => '30 points',
            ],

        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}
