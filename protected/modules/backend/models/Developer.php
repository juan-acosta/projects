<?php

/**
 * Model class for table developer.
 *
 * The columns in table:
 * @property integer $id
 * @property string $names
 * @property string $username
 * @property string $passwd
 * @property string $profile
 * @property string $email
 * @property string $created_at
 * @property integer $created_by
 * @property integer $status_id
 *
 * The model relations:
 * @property Task[] $tasks
 * @property Project[] $projects
 * @property UserStory[] $userStories
 * @property Bug[] $bugs
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Developer extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    const STATUS_NOACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;

    const ROL_ADMIN = 'admin';
    const ROL_OWNER = 'owner';
    const ROL_DEV = 'developer';
    const ROL_CLIENT = 'cliente';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'developer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['names, username, passwd, email', 'required'],
            ['created_by, status_id', 'numerical', 'integerOnly'=>true],
            ['names, passwd', 'length', 'max'=>250],
            ['username', 'length', 'max'=>25],
            ['profile', 'length', 'max'=>10],
            ['email', 'length', 'max'=>128],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, names, username, passwd, profile, email, created_at, created_by, status_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'tasks' => [self::HAS_MANY, 'Task', 'developer_id'],
            'projects' => [self::HAS_MANY, 'Project', 'developer_id'],
            'userStories' => [self::HAS_MANY, 'UserStory', 'developer_id'],
            'bugs' => [self::HAS_MANY, 'Bug', 'developer_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'names' => 'Nombres completos',
            'username' => 'Usuario',
            'passwd' => 'Clave',
            'profile' => 'Rol',
            'email' => 'Email',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'status_id' => 'Estado',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('names',$this->names,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('passwd',$this->passwd,true);
        $criteria->compare('profile',$this->profile,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('status_id',$this->status_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Developer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

    /**
     * Alias array
     *
     * @param string $type
     * @param null $code
     * @return bool|mixed
     */
    public static function itemAlias($type, $code=NULL)
    {
        $_items = [
            'status' => [
                self::STATUS_NOACTIVE => 'Inactivo',
                self::STATUS_ACTIVE => 'Activo',
                self::STATUS_BANNED => 'Bloqueado',
            ],

            'roles' => [
                self::ROL_ADMIN => 'Root',
                self::ROL_CLIENT => 'Cliente',
                self::ROL_DEV => 'Developer',
                self::ROL_OWNER => 'Responsable',
            ],

        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'owners' => [
                'condition' => ' profile in ( \''.self::ROL_OWNER.'\', \''.self::ROL_ADMIN.'\' )',
            ],
            'devs' => [
                'condition' => ' profile in ( \''.self::ROL_DEV.'\', \''.self::ROL_ADMIN.'\' )',
            ],

        ];
    }

}
