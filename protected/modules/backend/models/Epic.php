<?php

/**
 * Model class for table epic.
 *
 * The columns in table:
 * @property integer $id
 * @property string $epic
 * @property string $created_at
 * @property integer $created_by
 * @property integer $package_id
 *
 * The model relations:
 * @property UserStory[] $userStories
 * @property Package $package
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Epic extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;
    public $epic_with_package;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'epic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['created_by, package_id', 'numerical', 'integerOnly'=>true],
            ['epic', 'length', 'max'=>128],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, epic, created_at, created_by, package_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'userStories' => [self::HAS_MANY, 'UserStory', 'epic_id'],
            'package' => [self::BELONGS_TO, 'Package', 'package_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'epic' => 'Epic',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'package_id' => 'Package',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('epic',$this->epic,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('package_id',$this->package_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Epic the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);
        $this->epic_with_package = $this->epic .' - '.$this->package->package;

        return parent::afterFind();
    }

}
