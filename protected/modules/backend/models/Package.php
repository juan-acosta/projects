<?php

/**
 * Model class for table package.
 *
 * The columns in table:
 * @property integer $id
 * @property string $package
 * @property string $code
 * @property string $created_at
 * @property integer $created_by
 * @property integer $project_id
 *
 * The model relations:
 * @property Project $project
 * @property Epic[] $epics
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Package extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'package';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['created_by, project_id', 'numerical', 'integerOnly'=>true],
            ['package', 'length', 'max'=>128],
            ['code', 'length', 'max'=>16],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, package, code, created_at, created_by, project_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            ['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            ['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'project' => [self::BELONGS_TO, 'Project', 'project_id'],
            'epics' => [self::HAS_MANY, 'Epic', 'package_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package' => 'Paquete',
            'code' => 'Alias',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'project_id' => 'Proyecto',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('package',$this->package,true);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('project_id',$this->project_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Package the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

}
