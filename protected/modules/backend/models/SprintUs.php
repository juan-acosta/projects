<?php

/**
 * Model class for table sprint_us.
 *
 * The columns in table:
 * @property integer $id
 * @property integer $sprint_id
 * @property integer $us_id
 * @property string $cretaed_at
 * @property integer $created_by
 *
 * The model relations:
 * @property Task[] $tasks
 * @property Bug[] $bugs
 * @property Sprint $sprint
 * @property UserStory $us
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class SprintUs extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sprint_us';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['sprint_id, us_id, created_by', 'numerical', 'integerOnly'=>true],
            ['cretaed_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, sprint_id, us_id, cretaed_at, created_by', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            //['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            //['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'tasks' => [self::HAS_MANY, 'Task', 'sprint_us_id'],
            'bugs' => [self::HAS_MANY, 'Bug', 'sprint_us_id'],
            'sprint' => [self::BELONGS_TO, 'Sprint', 'sprint_id'],
            'us' => [self::BELONGS_TO, 'UserStory', 'us_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sprint_id' => 'Sprint ID',
            'us_id' => 'User Story ID',
            'cretaed_at' => 'Creado el',
            'created_by' => 'Creado por',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('sprint_id',$this->sprint_id);
        $criteria->compare('us_id',$this->us_id);
        $criteria->compare('cretaed_at',$this->cretaed_at,true);
        $criteria->compare('created_by',$this->created_by);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return SprintUs the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

}
