<?php

/**
 * Model class for table task.
 *
 * The columns in table:
 * @property integer $id
 * @property string $task
 * @property integer $developer_id
 * @property integer $hours_complete
 * @property integer $anio
 * @property integer $semana
 * @property integer $user_story_id
 * @property string $created_at
 * @property integer $created_by
 * @property integer $sprint_us_id
 *
 * The model relations:
 * @property Developer $developer
 * @property UserStory $userStory
 * @property SprintUs $sprintUs
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class Task extends CActiveRecord
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'task';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['task, developer_id, anio, semana', 'required'],
            ['developer_id, hours_complete, anio, semana, user_story_id, created_by, sprint_us_id', 'numerical', 'integerOnly'=>true],
            ['created_at', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, task, developer_id, hours_complete, anio, semana, user_story_id, created_at, created_by, sprint_us_id', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            //['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            //['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'developer' => [self::BELONGS_TO, 'Developer', 'developer_id'],
            'userStory' => [self::BELONGS_TO, 'UserStory', 'user_story_id'],
            'sprintUs' => [self::BELONGS_TO, 'SprintUs', 'sprint_us_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task' => 'Tarea',
            'developer_id' => 'Developer ID',
            'hours_complete' => 'Horas trabajo',
            'anio' => 'Anio',
            'semana' => 'Semana',
            'user_story_id' => 'Historia ID - FK',
            'created_at' => 'Creado el',
            'created_by' => 'Creado por',
            'sprint_us_id' => 'Sprint User Story ID',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria->compare('id',$this->id);
        $criteria->compare('task',$this->task,true);
        $criteria->compare('developer_id',$this->developer_id);
        $criteria->compare('hours_complete',$this->hours_complete);
        $criteria->compare('anio',$this->anio);
        $criteria->compare('semana',$this->semana);
        $criteria->compare('user_story_id',$this->user_story_id);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('sprint_us_id',$this->sprint_us_id);

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Task the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

}
