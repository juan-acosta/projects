<?php
/** @var $this PackageController */
/** @see backend/package/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Packages';
$this->headTitle = 'Administrar Packages';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/backend/package/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'Packages' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'package-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                'name'=>'project_id',
                //'header'=>'project_id',
                'value'=>function($row,$data){
                    return $row->project->project;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'package',
                //'header'=>'package',
                //'value'=>'package',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'code',
                //'header'=>'code',
                //'value'=>'code',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            /*
            'created_by',
            'project_id',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/package/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/package/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
