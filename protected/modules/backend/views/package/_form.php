<?php
/** @var $this PackageController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'package-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('project_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'project_id', ['class'=>'']); ?>
        </div>
        <div class="eight wide field">
            <?= $form->dropDownList($model, 'project_id', CHtml::listData(Project::model()->findAll(), 'id' ,'project'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'project_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('package')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'package', ['class'=>'']); ?>
        </div>
        <div class="eight wide field">
            <?= $form->textField($model,'package',['class'=>'', ]); ?>
            <?= $form->error($model,'package',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('code')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'code', ['class'=>'']); ?>
        </div>
        <div class="four wide field">
            <?= $form->textField($model,'code',['class'=>'', ]); ?>
            <?= $form->error($model,'code',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/backend/package'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

<?php

Yii::app()->clientScript->registerScript('backend_package_form', "

  $('#Package_project_id').dropdown({
    clearable: true
  });

", CClientScript::POS_END);