<?php
/** @var $this ProjectController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'project-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('client_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'client_id', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->dropDownList($model, 'client_id', CHtml::listData(Client::model()->findAll(), 'id' ,'ruc_name'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'client_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('project')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'project', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'project',['class'=>'', ]); ?>
            <?= $form->error($model,'project',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('abstract')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'abstract', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textArea($model,'abstract',['class'=>'', ]); ?>
            <?= $form->error($model,'abstract',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('https_git')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'https_git', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'https_git',['class'=>'', ]); ?>
            <?= $form->error($model,'https_git',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('developer_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'developer_id', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->dropDownList($model, 'developer_id', CHtml::listData(Developer::model()->owners()->findAll(), 'id' ,'username'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'developer_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>


    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/backend/project'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

<?php

Yii::app()->clientScript->registerScript('backend_project_form', "

  $('#Project_client_id').dropdown({
    clearable: true
  });
  $('#Project_developer_id').dropdown({
    clearable: true
  });

", CClientScript::POS_END);