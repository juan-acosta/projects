<?php
/** @var $this ProjectController */
/** @see backend/project/update [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'Projects';
$this->headTitle = 'Actualizar el/la Project ID#'.$model->id;

$this->breadcrumbs = [
    'Projects' => ['index'],
    'Actualizar',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Actualizar los datos del formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model]); ?>
    </div>
</div>