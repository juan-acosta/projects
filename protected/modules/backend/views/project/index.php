<?php
/** @var $this ProjectController */
/** @see backend/project/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Projects';
$this->headTitle = 'Administrar Projects';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/backend/project/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'Projects' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'project-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                'name'=>'client_id',
                //'header'=>'client_id',
                'value'=>function($row,$data){
                    return $row->client->ruc_name;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'project',
                //'header'=>'project',
                //'value'=>'project',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'abstract',
                //'header'=>'abstract',
                'value'=>'nl2br($data->abstract)',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'developer_id',
                //'header'=>'developer_id',
                'value'=>function($row,$data){
                    return $row->developer->username;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'https_git',
                'value'=>function($row,$data){
                    $_url_array = explode('/', $row->https_git);
                    return CHtml::link(end($_url_array), $row->https_git , ['target'=>'_blank']);
                },
                //'header'=>'https_git',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            /*
            'https_git',
            'created_at',
            'created_by',
            'client_id',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/project/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/project/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
