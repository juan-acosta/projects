<?php
/** @var $this UserStoryController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'user-story-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('epic_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'epic_id', ['class'=>'']); ?>
        </div>
        <div class="eight wide field">
            <?= $form->dropDownList($model, 'epic_id', CHtml::listData(Epic::model()->findAll(), 'id' ,'epic_with_package'), ['class'=>'ui search dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'epic_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('storie')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'storie', ['class'=>'']); ?>
        </div>
        <div class="ten wide field">
            <?= $form->textArea($model,'storie',['class'=>'', ]); ?>
            <?= $form->error($model,'storie',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('developer_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'developer_id', ['class'=>'']); ?>
        </div>
        <div class="eight wide field">
            <?= $form->dropDownList($model, 'developer_id', CHtml::listData(Developer::model()->devs()->findAll(), 'id' ,'username'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'developer_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('hours_estimated')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'hours_estimated', ['class'=>'']); ?>
        </div>
        <div class="two wide field">
            <?= $form->textField($model,'hours_estimated',['class'=>'', ]); ?>
            <?= $form->error($model,'hours_estimated',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('inicio_date')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'inicio_date', ['class'=>'']); ?>
        </div>
        <div class="three wide field">
            <?= $form->dateField($model,'inicio_date',['class'=>'', ]); ?>
            <?= $form->error($model,'inicio_date',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('compromiso_date')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'compromiso_date', ['class'=>'']); ?>
        </div>
        <div class="three wide field">
            <?= $form->dateField($model,'compromiso_date',['class'=>'', ]); ?>
            <?= $form->error($model,'compromiso_date',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/backend/userStory'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

<?php

Yii::app()->clientScript->registerScript('backend_userstory_form', "

  $('#UserStory_epic_id').dropdown({
    clearable: true
  });
  $('#UserStory_developer_id').dropdown({
    clearable: true
  });

", CClientScript::POS_END);