<?php
/** @var $this UserStoryController */
/** @see backend/userStory/update [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'User Stories';
$this->headTitle = 'Actualizar el/la UserStory ID#'.$model->id;

$this->breadcrumbs = [
    'User Stories' => ['index'],
    'Actualizar',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Actualizar los datos del formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model]); ?>
    </div>
</div>