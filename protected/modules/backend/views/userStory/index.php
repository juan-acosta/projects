<?php
/** @var $this UserStoryController */
/** @see backend/userStory/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'User Stories';
$this->headTitle = 'Administrar User Stories';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/backend/userStory/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'User Stories' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'user-story-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                //'name'=>'epic_id',
                'header'=>'Project',
                'value'=>function($row,$data){
                    return $row->epic->package->project->project ;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                //'name'=>'epic_id',
                'header'=>'Scrum Master',
                'value'=>function($row,$data){
                    return $row->epic->package->project->developer->names ;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                //'name'=>'epic_id',
                'header'=>'Package',
                'value'=>function($row,$data){
                    return $row->epic->package->code .'<br/>'.$row->epic->package->package;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'epic_id',
                //'header'=>'epic_id',
                'value'=>function($row,$data){
                    return $row->epic->epic;
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'storie',
                //'header'=>'storie',
                'value'=>function($row,$data){
                    return nl2br($row->storie) .'<br/><strong>* Developer: '.$row->developer->username.'</strong>';
                },
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'percent_finish',
                //'header'=>'percent_finish',
                //'value'=>'percent_finish',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'inicio_date',
                //'header'=>'inicio_date',
                //'value'=>'inicio_date',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'compromiso_date',
                //'header'=>'compromiso_date',
                //'value'=>'compromiso_date',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ], [
                //'class'=>'DataColumn',
                'name'=>'hours_estimated',
                //'header'=>'hours_estimated',
                //'value'=>'hours_estimated',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            /*
            'compromiso_date',
            'inicio_date',
            'hours_estimated',
            'hours_effective',
            'percent_finish',
            'created_at',
            'created_by',
            'epic_id',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/userStory/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/userStory/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
