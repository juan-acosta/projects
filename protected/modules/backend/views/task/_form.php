<?php
/** @var $this TaskController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'task-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('task')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'task', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'task',['class'=>'', ]); ?>
            <?= $form->error($model,'task',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('developer_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'developer_id', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'developer_id',['class'=>'', ]); ?>
            <?= $form->error($model,'developer_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('hours_complete')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'hours_complete', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'hours_complete',['class'=>'', ]); ?>
            <?= $form->error($model,'hours_complete',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('anio')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'anio', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'anio',['class'=>'', ]); ?>
            <?= $form->error($model,'anio',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('semana')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'semana', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'semana',['class'=>'', ]); ?>
            <?= $form->error($model,'semana',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('user_story_id')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'user_story_id', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'user_story_id',['class'=>'', ]); ?>
            <?= $form->error($model,'user_story_id',['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/backend/task'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
