<?php
/** @var $this TaskController */
/** @see backend/task/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Tasks';
$this->headTitle = 'Administrar Tasks';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/backend/task/create'],
    'color' => 'primary',
];

$this->breadcrumbs = [
    'Tasks' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?php $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'task-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
            [
                //'class'=>'DataColumn',
                'name'=>'id',
                //'header'=>'id',
                //'value'=>'id',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'task',
                //'header'=>'task',
                //'value'=>'task',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'developer_id',
                //'header'=>'developer_id',
                //'value'=>'developer_id',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            [
                //'class'=>'DataColumn',
                'name'=>'hours_complete',
                //'header'=>'hours_complete',
                //'value'=>'hours_complete',
                'type'=>'raw',
                //'filter'=>false,
                'htmlOptions'=>['class'=>''],
            ],
            /*
            'anio',
            'semana',
            'user_story_id',
            'created_at',
            'created_by',
            */
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/task/delete/",["id"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/backend/task/update/",["id"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
