<?php
/* @var $this SecurityController */
$this->pageTitle = 'Login';
$baseUrl = Yii::app()->baseUrl;
?>

<h1 class="ui blue image header">
    <img class="image" src="<?= $baseUrl?>/images/logo_trans.png" />
    <div class="content">
        Ingresar Credenciales
    </div>
</h1>

<!-- form class="ui large form" -->

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'login-form',
    'enableAjaxValidation'=>false,
    'errorMessageCssClass' => 'error',
    'htmlOptions' => [
        'id' => 'login-validation',
        'novalidate'=>'novalidate',
        'autocomplete'=>"off",
        'role'=>'form',
        'class' => 'ui fluid form',
    ]
]); ?>

    <div class="ui stacked segment left aligned">

        <div class="field <?= $model->hasErrors('username')?'error':'';?>">
            <div class="ui left icon input">
                <i class="user icon"></i>
                <?= $form->textField($model,'username',['class'=>'form-control', 'placeholder'=>'Usuario' ]); ?>
            </div>
            <?= $form->error($model,'username', ['class'=>'ui pointing red basic label']); ?>
        </div>

        <div class="field <?= $model->hasErrors('password')?'error':'';?>">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <?= $form->passwordField($model,'password',['class'=>'form-control', 'placeholder'=>'Clave' ]); ?>
            </div>
            <?= $form->error($model,'password', ['class'=>'ui pointing red basic label']); ?>
        </div>

        <button type="submit" class="ui fluid large blue submit button">Login</button>

    </div>

<!--/form-->
<?php $this->endWidget(); ?>

