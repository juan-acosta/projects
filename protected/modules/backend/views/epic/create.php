<?php
/** @var $this EpicController */
/** @see backend/epic/create [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'Epics';
$this->headTitle = 'Crear Epic';

$this->breadcrumbs = [
    'Epics' => ['index'],
    'Crear',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Completar el formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model] ); ?>
    </div>
</div>