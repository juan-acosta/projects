<?php
/** @var $this EpicController */
/** @see backend/epic/update [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = 'Epics';
$this->headTitle = 'Actualizar el/la Epic ID#'.$model->id;

$this->breadcrumbs = [
    'Epics' => ['index'],
    'Actualizar',
];
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Actualizar los datos del formulario:</h4>
        <?= $this->renderPartial('_form', ['model'=>$model]); ?>
    </div>
</div>