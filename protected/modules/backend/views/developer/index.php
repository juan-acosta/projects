<?php
/** @var $this DeveloperController */
/** @see backend/developer/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = 'Developers';
$this->headTitle = 'Administrar Developers';

$this->btnActions[] = [
    'name' => '<i class="icon plus"></i> Crear',
    'url' => ['/backend/developer/create'],
    'color' => 'blue',
];

$this->breadcrumbs = [
    'Developers' => ['index'],
    'Administrar',
];

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>3];
?>



<?php $this->widget('zii.widgets.grid.CGridView', [
    'id'=>'developer-grid',
    'dataProvider' => $dataProvider,
    //'filter' => $model,
    //'itemsCssClass' => 'ui celled structured striped table',
    //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
    'columns' => [
        [
            //'class'=>'DataColumn',
            'name'=>'names',
            //'header'=>'names',
            //'value'=>'names',
            'type'=>'raw',
            //'filter'=>false,
            'htmlOptions'=>['class'=>''],
        ], [
            //'class'=>'DataColumn',
            'name'=>'profile',
            //'header'=>'profile',
            'value'=>'Developer::itemAlias("roles", $data->profile)',
            'type'=>'raw',
            //'filter'=>false,
            'htmlOptions'=>['class'=>''],
        ],
        [
            //'class'=>'DataColumn',
            'name'=>'username',
            //'header'=>'username',
            //'value'=>'username',
            'type'=>'raw',
            //'filter'=>false,
            'htmlOptions'=>['class'=>''],
        ],
        [
            //'class'=>'DataColumn',
            'name'=>'email',
            //'header'=>'email',
            //'value'=>'email',
            'type'=>'raw',
            //'filter'=>false,
            'htmlOptions'=>['class'=>''],
        ],
        /*
        'profile',
        'email',
        'created_at',
        'created_by',
        'status_id',
        */
        [
            'class'=>'CButtonColumn',
            'header' => '',
            'template' => '{update} {delete}',
            'buttons' => [
                'delete' => [
                    'label' => '<i class="trash alternate outline icon"></i>',
                    'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                    'imageUrl' => false,
                ],
                'update' => [
                    'label' => '<i class="edit outline icon"></i>',
                    'options' => ['title'=>'Actualizar', 'class'=>'ui compact teal icon button'],
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("/backend/developer/update/",["id"=>$data->id_crypt])'
                ]
            ],
            //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
            'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
        ],
    ],
]); ?>

