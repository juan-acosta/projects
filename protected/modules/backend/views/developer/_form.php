<?php
/** @var $this DeveloperController */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?php
$form=$this->beginWidget('CActiveForm', [
    'id'=>'developer-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[$model,'<one_filed_primary>'],
]); ?>

    <?php //echo $form->errorSummary($model, '',''); ?>

    <div class="fields <?= $model->hasErrors('names')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'names', ['class'=>'']); ?>
        </div>
        <div class="thirteen wide field">
            <?= $form->textField($model,'names',['class'=>'', ]); ?>
            <?= $form->error($model,'names', ['class'=>' ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('username')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'username', ['class'=>'', ]); ?>
        </div>
        <div class="five wide field">
            <?= $form->textField($model,'username', ['class'=>'', ]); ?>
            <?= $form->error($model,'username', ['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('passwd')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'passwd', ['class'=>'', ]); ?>
        </div>
        <div class="five wide field">
            <?= $form->passwordField($model,'passwd',['class'=>'', ]); ?>
            <?= $form->error($model,'passwd', ['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('profile')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'profile', ['class'=>'', ]); ?>
        </div>
        <div class="four wide field">
            <?= $form->dropDownList($model, 'profile', Developer::itemAlias('roles'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'profile', ['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('email')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'email', ['class'=>'', ]); ?>
        </div>
        <div class="eight wide field">
            <?= $form->textField($model,'email',['class'=>'', ]); ?>
            <?= $form->error($model,'email', ['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <div class="fields <?= $model->hasErrors('status_id')?'has-error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= $form->labelEx($model, 'status_id', ['class'=>'', ]); ?>
        </div>
        <div class="four wide field">
            <?= $form->dropDownList($model, 'status_id', Developer::itemAlias('status'), ['class'=>'ui fluid dropdown', 'empty'=>'Seleccionar..', ]) ?>
            <?= $form->error($model,'status_id', ['class'=>'ui pointing red basic label', ]); ?>
        </div>
    </div>

    <br>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= $model->isNewRecord ? 'Grabar' : 'Actualizar';?>
            </button>
            <?= CHtml::link('Cancelar', ['/backend/developer'], ['class'=>'mini ui button']); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
