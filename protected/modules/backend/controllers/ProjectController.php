<?php
/**
 * Add Description for ProjectController
 *
 ** Using the model Project (backend.models.Project)
 *
 * @var $this ProjectController
 * @package __ModuleName__
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */

class ProjectController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return [
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        ];
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            ['allow', // allow authenticated user
                'actions' => ['index', 'create', 'update', 'delete'],
                'users' => ['@'],
                //'expression' => 'Yii::app()->user->checkAccess([Usuario::ROL_ADMIN])',
            ],
            ['deny',  // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Project;

        // send POST
        if(isset($_POST['Project']))
        {
            // obtener los parametros via POST
            $model->attributes=$_POST['Project'];

            // registrar
            if( $model->save() )
            {
                // by default
                $this->redirect(['index']);
                // Redirection with security -- optional
                //$this->redirect( ['/backend/project/<method>]', 'id'=>$model->id_crypt]);
            }
        }

        $this->render('create', [
            'model'=>$model,
        ]);
    }

    /**
     * Updates a particular model.
     * @param integer $id the ID of the model to be updated
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        // decodificar model id_crypt -- optional
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);
        $id = $crypt->decrypt($id); // ID decrypt

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Project']))
        {
            $model->attributes=$_POST['Project'];

            if( $model->save() )
            {
                // by default
                $this->redirect(['index']);
                // Redirection with security -- optional
                //$this->redirect( ['/backend/project/<method>]', 'id'=>$model->id_crypt]);
            }
        }

        $this->render('update', [
            'model'=>$model,
        ]);
    }

    /**
     * Deletes a particular model.
     * @param integer $id the ID of the model to be deleted
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        // decodificar model id_crypt -- optional
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);
        $id = $crypt->decrypt($id); // ID decrypt

        // eliminar de la BD
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model=new Project('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Project']))
            $model->attributes=$_GET['Project'];

        if( Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) &&  $_GET['ajax'] === 'project-grid' ){
            $this->renderPartial('index', ['model'=>$model]);
            Yii::app()->end();
        }

        $this->render('index', [
            'model'=>$model,
        ]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Project the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Project::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Project $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
