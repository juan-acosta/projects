<?php

/**
 * Package Security - Login
 *
 * @var $this SecurityController
 * @package BackendModule
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class SecurityController extends Controller
{
    public $defaultAction = 'login';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return [
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        ];
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            ['allow', // allow authenticated user
                'actions' => ['login'],
                'users' => ['*'],
            ],
            ['allow', // allow authenticated user
                'actions' => ['logout'],
                'users' => ['@'],
            ],
            ['deny',  // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Login
     */
	public function actionLogin()
	{
        // set layout
        $this->layout = 'application.views.layouts.login';

        if (Yii::app()->user->isGuest)
        {

            $model = new LoginForm;

            // collect user input data
            if(isset($_POST['LoginForm']))
            {
                $model->attributes=$_POST['LoginForm'];

                // validate user input and redirect to previous page if valid
                if( $model->validate() )
                {
                    $this->redirect(Yii::app()->getModule('backend')->returnUrl);
                }
            }

            $this->render('login', ['model'=>$model]);

        } else {
            $this->redirect(Yii::app()->getModule('backend')->returnUrl);
        }

	}

    /**
     * Logout
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->getModule('backend')->returnLogoutUrl);
    }

}