<?php
/**
 * Add Description for SprintUsController
 *
 ** Using the model SprintUs (backend.models.SprintUs)
 *
 * @var $this SprintUsController
 * @package __ModuleName__
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */

class SprintUsController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return [
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        ];
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            ['allow', // allow authenticated user
                'actions' => ['index', 'create', 'update', 'delete'],
                'users' => ['@'],
                //'expression' => 'Yii::app()->user->checkAccess([Usuario::ROL_ADMIN])',
            ],
            ['deny',  // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new SprintUs;

        // send POST
        if(isset($_POST['SprintUs']))
        {
            // obtener los parametros via POST
            $model->attributes=$_POST['SprintUs'];

            // registrar
            if( $model->save() )
            {
                // by default
                $this->redirect(['index']);
                // Redirection with security -- optional
                //$this->redirect( ['/backend/sprintUs/<method>]', 'id'=>$model->id_crypt]);
            }
        }

        $this->render('create', [
            'model'=>$model,
        ]);
    }

    /**
     * Updates a particular model.
     * @param integer $id the ID of the model to be updated
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        // decodificar model id_crypt -- optional
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);
        $id = $crypt->decrypt($id); // ID decrypt

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['SprintUs']))
        {
            $model->attributes=$_POST['SprintUs'];

            if( $model->save() )
            {
                // by default
                $this->redirect(['index']);
                // Redirection with security -- optional
                //$this->redirect( ['/backend/sprintUs/<method>]', 'id'=>$model->id_crypt]);
            }
        }

        $this->render('update', [
            'model'=>$model,
        ]);
    }

    /**
     * Deletes a particular model.
     * @param integer $id the ID of the model to be deleted
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        // decodificar model id_crypt -- optional
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);
        $id = $crypt->decrypt($id); // ID decrypt

        // eliminar de la BD
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     *
     * @param string $code week-anio
     * @throws CException
     * @throws CHttpException
     */
    public function actionIndex($code='')
    {
        // code for Sprint
        $_row_sprint = Sprint::model()->findByAttributes(['week_id'=>$code]);

        if( !$_row_sprint )
            throw new CHttpException(404,'The requested value does not exist.');

        // model
        $model=new SprintUs('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['SprintUs']))
            $model->attributes=$_GET['SprintUs'];

        // set force
        $model->sprint_id = $_row_sprint->id ;

        if( Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) &&  $_GET['ajax'] === 'sprint-us-grid' ){
            $this->renderPartial('index', ['model'=>$model]);
            Yii::app()->end();
        }

        // next week
        $now = new DateTime;
        $interval = new DateInterval('P1W') ;
        $next_week = $now->add($interval);

        $_code_next = (int)$next_week->format('W').'-'.$next_week->format('Y');

        $this->render('index', [
            'model'=>$model,
            'code'=>$code,
            'code_next'=>$_code_next,
        ]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SprintUs the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=SprintUs::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SprintUs $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='sprint-us-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
