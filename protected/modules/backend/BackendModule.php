<?php

/**
 * Description for BackendModule
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class BackendModule extends CWebModule
{
    /** @var bool  */
    public $sendActivationMail=true;

    /** @var boolean $loginNotActiv allow auth for is not active user */
    public $loginNotActiv=false;

    /** @var boolean $activeAfterRegister activate user on registration (only $sendActivationMail = false) */
    public $activeAfterRegister=false;

    /** @var boolean $autoLogin login after registration (need loginNotActiv or activeAfterRegister = true) */
    public $autoLogin=true;

    /** @var bool  */
    public $activeLoginUrl = false;

    /** @var bool  */
    public $activeRegisterUrl = false;

    /** @var bool  */
    public $activeRecoveryUrl = false;

    /** @var array  */
    public $returnUrl = ["/backend/default"];

    /** @var array  */
    public $returnLogoutUrl = ["/backend/security"];

    /** @var int 30 days */
    public $rememberMeTime = 2592000;

    /** @var int 1 day */
    public $defaultTime = 86400;

    public function init()
    {
        // import the module-level models and components
        $this->setImport([
            'backend.models.*',
            'backend.components.*',
        ]);
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @return hash string.
     */
    public static function encrypting($string="")
    {
        // sha512 => 128 long
        return password_hash($string, PASSWORD_BCRYPT, ['cost' => 10] );
    }


    /**
     * @param $hash
     * @return bool
     */
    public static function verify_hash($string, $hash)
    {

        return password_verify($string, $hash);
    }



}
