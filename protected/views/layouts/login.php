<?php /* @var $this Controller */ ?>
<?php $baseUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="language" content="en">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=$baseUrl?>/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>/images/favicon-16x16.png">
    <link rel="shortcut icon" href="<?= $baseUrl;?>/images/favicon.ico">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/semantic/dist/semantic.min.css">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/semantic/dist/semantic.min.js"></script>

	<title><?= CHtml::encode($this->pageTitle) .' | '. Yii::app()->name; ?></title>

</head>

<body>

<div class="ui middle aligned center aligned grid">
    <div class="column">
        <?= $content; ?>
    </div>
</div>


<style type="text/css">
    body {
        background-color: #DADADA;
    }
    body > .grid {
        height: 100%;
    }
    .image {
        margin-top: -100px;
    }
    .column {
        max-width: 450px;
    }
</style>

</body>
</html>
