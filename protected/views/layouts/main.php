<?php /* @var $this Controller */ ?>
<?php $baseUrl = Yii::app()->baseUrl; ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="language" content="en">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=$baseUrl?>/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>/images/favicon-16x16.png">
    <link rel="shortcut icon" href="<?= $baseUrl;?>/images/favicon.ico">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/semantic/dist/semantic.min.css">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/semantic/dist/semantic.min.js"></script>

	<title><?= CHtml::encode($this->pageTitle) .' | '. Yii::app()->name; ?></title>

</head>

<body>

<!-- Following Menu -->
<div class="ui fixed inverted menu">
    <div class="ui container">
        <?php $this->widget('application.components.DMenu',array(
            'encodeLabel'=>false,
            'items'=>[
                ['label'=>'<i class="home icon"></i> Home', 'url'=>['/backend/default/index'], ],
                ['label'=>'Developer', 'url'=>['/backend/developer'], 'visible' => Yii::app()->user->checkAccess( [ Developer::ROL_OWNER ], [ 'restrict' => false ]) ],
                ['label'=>'Clientes', 'url'=>['/frontend/client'], 'visible' => Yii::app()->user->checkAccess( Developer::ROL_ADMIN ) ],
                ['label'=>'Contactos', 'url'=>['/frontend/contact'], 'visible' => Yii::app()->user->checkAccess( Developer::ROL_ADMIN ) ],
                ['label'=>'Sprint', 'url'=>['/backend/sprintUs/index/code/'.(int)date('W').'-'.date('Y')], 'visible' => Yii::app()->user->checkAccess( Developer::ROL_ADMIN ) ],
            ],
        )); ?>
        <div class="right menu">
            <div class="item">
                <?php if( Yii::app()->user->isGuest ): ?>
                <?= CHtml::link('Login', ['/backend/security'], [ 'class'=>'ui button'] ); ?>
                <?php else: ?>
                    <?= CHtml::link('<i class="sign-out icon"></i> Salir', ['/backend/security/logout'], [ 'class'=>'ui button mini'] ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<div class="ui main container">

    <div class="ui grid">
        <div class="ten wide column">
            <h1 class="ui header">
                <?= $this->headTitle ?>
            </h1>

            <?php // establecer los enlaces de seguimiento
            if( isset($this->breadcrumbs) ){
                $this->widget('zii.widgets.CBreadcrumbs', [
                    'links'=>$this->breadcrumbs,
                    'homeLink'=> CHtml::link('Inicio', ['/backend/default'], ['class'=>'section'] ),
                    'tagName'=>'div',
                    'separator'=>'<i class="right angle icon divider"></i>',
                    'activeLinkTemplate'=>'<a class="section" href="{url}">{label}</a>',
                    'inactiveLinkTemplate'=>'<div class="active section">{label}</div>',
                    'htmlOptions'=>['class'=>'ui small breadcrumb','style'=>'']
                ]);
            } ?>
        </div>
        <div class="six wide column right aligned">
            <?php // establecer los botones de accion
            if(isset($this->btnActions)){
                foreach( $this->btnActions as $btn ){
                    $_view_btn = isset($btn['view']) ? $btn['view'] : true;
                    if( $_view_btn ){
                        echo CHtml::link($btn['name'], $btn['url'],
                            ['class'=>' ui button tiny '.$btn['color'], 'style'=>'margin: 0 5px;']
                        );
                    }
                }
            } ?>
        </div>
    </div>

    <div class="ui hidden divider"></div>

    <?= $content; ?>

</div>

<div class="ui inverted vertical footer segment">
    <div class="ui container">
        <div class="ui stackable inverted divided equal height stackable grid">
            <div class="three wide column">
                <h4 class="ui inverted header">About</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Sitemap</a>
                    <a href="#" class="item">Contact Us</a>
                    <a href="#" class="item">Religious Ceremonies</a>
                    <a href="#" class="item">Gazebo Plans</a>
                </div>
            </div>
            <div class="three wide column">
                <h4 class="ui inverted header">Services</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Banana Pre-Order</a>
                    <a href="#" class="item">DNA FAQ</a>
                    <a href="#" class="item">How To Access</a>
                    <a href="#" class="item">Favorite X-Men</a>
                </div>
            </div>
            <div class="seven wide column">
                Copyright &copy; <?php echo date('Y'); ?> by DDAR.<br/>
                All Rights Reserved.<br/>
                <?php echo Yii::powered(); ?>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    body {
        background-color: #FFFFFF;
    }
    .ui.menu .item img.logo {
        margin-right: 1.5em;
    }
    .main.container {
        margin-top: 5em;
    }
    .wireframe {
        margin-top: 2em;
    }
    .ui.footer.segment {
        margin: 5em 0em 0em;
        padding: 5em 0em;
    }
</style>

</body>
</html>
