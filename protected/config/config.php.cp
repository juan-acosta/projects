<?php
/**
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */

 /** @var string $protocol Verificar si es HTTP o HTTPS */
 $protocol = isset($_SERVER["https"]) ? 'https://' : 'http://';

 /** @var array $_url_to_redirect Lista de URLs de los Modulos activos */
 $_url_to_redirect = [];

 /** @var array $_modules Lista de Modulos activos */
 $_modules = ['',];

 /** @var array $_domain_modules Lista de Dominios asociado a los Modulos activos */
 $_domain_modules = [];

 /** @var array $_structure_modules Lista de Modulos, Subdominios, Url */
 $_structure_modules = [
     'page' => [
         'id'        => 'url_frontend',
         'url'       => $protocol.'singlepage-{DEV}.ddar.pe',
         'module'    => 'site',
         'active'    => true
     ],
     'backend'=> [
         'id'        => 'url_backend',
         'url'       => $protocol.'admin-{DEV}.ddar.pe',
         'module'    => 'backend/default',
         'active'    => true
     ]
 ];

 // Recorrer matriz
 foreach( $_structure_modules as $k => $v)
 {
     if( $v['active'] )
     {
         $_url_to_redirect = $_url_to_redirect + [ $v['id'] => $v['url'] ];
         $_domain_modules = $_domain_modules + [ $v['url'] => $v['module'] ];
         $_modules[] = $k ;
    }
}

// Modulo Login/Security
/** @var array $_module_user Configuracion del Modulo de Usuario*/
$_module_user = [
    'backend' => [
        'sendActivationMail' => false,
        'loginNotActiv' => false,
        'activeAfterRegister' => false,
        'autoLogin' => true,
        'activeLoginUrl' => true,
        'activeRegisterUrl' => false,
        'activeRecoveryUrl' => false,
    ]
];

/** @var array $_gii_config Configuracion de Modulo GII */
$_gii_config = [
    'gii' => [
        'class'=>'system.gii.GiiModule',
        'password'=>'PASSWORD',
        'ipFilters'=>['127.0.0.1','::1'],
    ]
];

// Unificar Modulos
$_modules = $_module_user + $_gii_config + $_modules;

// Configuracion Parametros Globals
/** @var array $_params_global Parametros Globales del Proyecto */
$_params_global = [

    'title' => 'Proyectos DDAR',
    'email' => 'juan.acosta@ddar.pe',
    'description' => 'Gestion de Proyectos de Desarrollo Web',
    'keywords' => 'scrum, kamban, agile, yii, php',
    'version' => 'v1.0',
    'footer' => ' | - LOCAL - ',
    'home' => '/backend/default/',

    // Enable LOG WEB ROUTE
    'cweblog' => false,
    // Enable LOG FIREBUG
    'firebug' => false,
    // Enable DB Log
    'dblog' => false,

    // EMAIL LOGs
    'mails_danger' => 'juan.acosta@ddar.pe',
    'mail_soporte' => 'soporte@grupogtd.com',

    // mycrypt params
    'my_iv' => '', // 16 digits
    'my_passwd' => '', // strong string

];


// Unificar PARAMS
$_params_global = $_params_global + $_url_to_redirect ;

$db = [
    'class'=>'CDbConnection',
    'connectionString' => 'pgsql:host=_IP_;port=5432;dbname=intranet',
    'emulatePrepare' => true,
    'username' => '_DBUSER_',
    'password' => '_DBPASSWD_',
    'charset'  => 'UTF8',
];