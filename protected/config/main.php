<?php

/** Parametros de cofiguracion global */
require_once __DIR__ . '/config.php';

/** Composer autoloader */
$rootProject = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..';
$composerAutoloader = implode(DIRECTORY_SEPARATOR, [ $rootProject , 'vendor', 'autoload.php']);
require $composerAutoloader;

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name'=>$_params_global['title'],

    'language'=>'es',

	// preloading 'log' component
	'preload'=>['log'],

	// autoloading model and component classes
	'import'=>[
		'application.models.*',
		'application.modules.backend.models.*',
		'application.modules.frontend.models.*',
		'application.components.*',
	],

	'modules'=>$_modules,

    'aliases' => [],

	// application components
	'components'=>[

		'user'=>[
            'loginUrl' => ['/backend/security'],
            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'authTimeout' => 86400,
		],

		// uncomment the following to enable URLs in path-format
        'urlManager' => [
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' =>
                [
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ] + $_domain_modules
            ,
        ],


		// database settings are configured in database.php
		'db'=>$db,

		'errorHandler'=>[
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		],

		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			],
		],

        'clientScript' => [
            'scriptMap' => [
                'jquery.js'=>false,  //disable default implementation of jquery
                //'jquery.min.js'=>false,  //desable any others default implementation
                'grid.js'=>false,  //disable default implementation of jquery
                'jquery.ba-bbq.min.js' => false,
                'jquery.yiigridview.js' => false,
                'core.css'=>false, //disable
                'styles.css'=>false,  //disable
                'pager.css'=>false,   //disable
                'default.css'=>false,  //disable
            ]
        ],

        // TABLE STYLE
        'widgetFactory' => [
            'widgets' => [
                'CGridView' => [
                    'htmlOptions' => ['class' => 'ui stackable grid'],
                    'itemsCssClass' => 'ui celled structured striped attached table',
                    'template' => '<div class="sixteen wide column">{items}</div>{summary}{pager}',
                    'pager' => [
                        'class'    => 'DlinkPager',
                        'header'    => false,
                        'footer'    => false,
                        'cssFile'   => false,
                        'firstPageLabel'=> '<i class="angle double left icon"></i>',
                        'prevPageLabel' => '<i class="angle left icon"></i>',
                        'nextPageLabel' => '<i class="angle right icon"></i>',
                        'lastPageLabel' => '<i class="angle double right icon"></i>',
                        'htmlOptions'   => ['class'=>'ui pagination menu'],
                        'selectedPageCssClass' => 'active',
                        'internalPageCssClass'=>'',
                    ],
                    'pagerCssClass' => 'eight wide column right aligned',
                    'summaryText' => '{start} - {end} de {count} registros.',
                    'summaryCssClass' => 'eight wide column text-left',
                    //'summaryTagName' => '',
                    'enablePagination' => true,
                ]
            ]
        ],

	],

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => $_params_global,
];
