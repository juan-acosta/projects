<?php
/**
 * CMenu
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
Yii::import('zii.widgets.CMenu');

class Dmenu extends CMenu {

    /**
     * @param array $items
     */
    protected function renderMenu($items)
    {
        if(count($items))
        {
            //echo CHtml::openTag('ul',$this->htmlOptions)."\n";
            echo "\n";
            $this->renderMenuRecursive($items);
            //echo CHtml::closeTag('ul');
        }
    }

    /**
     * @param array $item
     * @return string
     */
    protected function renderMenuItem($item)
    {

        if( isset($item['url']) ) {
            $label = $this->linkLabelWrapper===null
                ? $item['label']
                : CHtml::tag( $this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label'] );

            return CHtml::link( $label, $item['url'], isset($item['linkOptions']) ? $item['linkOptions'] : [] );

        }
        else {
            return CHtml::tag('span', isset($item['linkOptions']) ? $item['linkOptions'] : [], $item['label']);
        }

    }

    /**
     * @param array $items
     */
    protected function renderMenuRecursive($items)
    {

        $count=0;

        // cuantos items existe
        $n=count($items);

        // recorrer los items
        foreach($items as $item)
        {
            $count++;

            $options = isset($item['itemOptions']) ? $item['itemOptions'] : [];

            $class=[];

            if($item['active'] && $this->activeCssClass!=''){
                $class[] = $this->activeCssClass;
                if( isset($item['active']) ){
                    $item['linkOptions']['class'] = 'item active';
                }

            } else {
                $item['linkOptions']['class'] = 'item';
            }

            if($count===1 && $this->firstItemCssClass!==null)
                $class[]=$this->firstItemCssClass;

            if($count===$n && $this->lastItemCssClass!==null)
                $class[]=$this->lastItemCssClass;

            if($this->itemCssClass!==null)
                $class[]=$this->itemCssClass;

            if($class!==array())
            {
                if(empty($options['class']))
                    $options['class']=implode(' ',$class);
                else
                    $options['class'].=' '.implode(' ',$class);
            }

            //echo CHtml::openTag('li', $options);

            $menu=$this->renderMenuItem($item);
            if(isset($this->itemTemplate) || isset($item['template']))
            {
                $template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template,array('{menu}'=>$menu));
            }
            else
                echo $menu;

            if(isset($item['items']) && count($item['items']))
            {
                //Indicarle al SubMenu que tiene un ID igual al titulo
                $this->submenuHtmlOptions['id'] = $item['linkOptions']['title'];

                echo "\n".CHtml::openTag(
                        'ul',
                        isset($item['submenuOptions'])
                            ? $item['submenuOptions']
                            : $this->submenuHtmlOptions)."\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag('ul')."\n";
            }

            //echo CHtml::closeTag('li')."\n";
            echo "\n";
        }
    }

}