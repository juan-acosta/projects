<?php


class DLinkPager extends CLinkPager {

    //.... your methods

    /**
     * Executes the widget.
     * This overrides the parent implementation by displaying the generated page buttons.
     */
    public function run()
    {
        $this->registerClientScript();
        $buttons=$this->createPageButtons();
        if(empty($buttons))
            return;
        echo $this->header;
        echo CHtml::tag('div',$this->htmlOptions,implode("\n",$buttons));
        echo $this->footer;
    }

    /**
     * Creates a page button.
     * You may override this method to customize the page buttons.
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $hidden whether this page button is visible
     * @param boolean $selected whether this page button is selected
     * @return string the generated button
     */
    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        $class .= ' item ';
        if($hidden || $selected)
            $class .='  '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        //return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>';
        return CHtml::link($label,$this->createPageUrl($page), ['class'=>$class]);
    }

}

