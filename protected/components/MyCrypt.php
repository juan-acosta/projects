<?php
/**
 * Encriptar / desencriptar registros.
 *
 * @author    Juan Acosta <juan.acosta@ddar.pe>
 * @copyright 2018 DDAR
 */

class MyCrypt
{

    private $passwd;
    private $iv;
    private $method = 'AES-128-CBC';


    function __construct(){}

    /**
     * Set private key
     * @param $string
     */
    function setPasswd($string)
    {
        $this->passwd = $string;
    }

    /**
     * Set IV Hash
     * @param $string
     */
    function setIv($string)
    {
        $this->iv = $string;
    }

    /**
     * Set IV Hash
     * @param $string
     */
    function setMethod($string)
    {
        $this->method = $string;
    }

    public function encrypt($string)
    {
        $_method = $this->method;
        $_passwd = $this->passwd;
        $_iv = $this->iv;

        return base64_encode(openssl_encrypt($string, $_method, $_passwd, 0, $_iv));

    }


    public function decrypt($string)
    {
        $_method = $this->method;
        $_passwd = $this->passwd;
        $_iv = $this->iv;

        return openssl_decrypt(base64_decode($string), $_method, $_passwd, 0, $_iv);

    }


}
