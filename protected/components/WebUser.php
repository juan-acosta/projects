<?php

/**
 * WebUser represents the data needed to assoc a user.
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class WebUser extends CWebUser
{

    private $_model;

    /**
     * @param IUserIdentity $identity
     * @param int $duration
     * @return bool|void
     * @throws CException
     */
    public function login($identity, $duration = 0)
    {
        parent::login($identity, $duration);
        $this->setId($identity->id);
    }

    public function setId($id) {
        $this->setState('__id', $id);
    }

    public function getId(){
        return $this->getState('__id') ;
    }

    /**
     * @param bool $fromCookie
     * @throws CHttpException
     */
    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->updateSession();
    }

    public function updateSession()
    {

        $_row_dev = Developer::model()->findByPk($this->id);

        $userAttributes = [
            'username' => $_row_dev->username,
            'names' => $_row_dev->names,
            'email' => $_row_dev->email,
            'profile' => $_row_dev->profile,
        ];

        // recorrer attributos
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }

    }


    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation
     * @param array $params
     * @param bool $allowCaching
     * @return bool|mixed Permission granted?
     */
    public function checkAccess($operation,$params=array(),$allowCaching=true)
    {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }

        // Upgrade call to filed ROLES
        $role = Yii::app()->user->profile;

        // saltar validacion
        if( $role == Developer::ROL_ADMIN ){
            if( isset( $params['restrict'] ) ){
                return !$params['restrict'];
            }
        }

        // allow access if the operation request is the current user's role
        if( !is_array($operation) )
            return ($operation === $role);
        else
            return ( in_array($role, $operation) ); //$operation === $role);
    }

}