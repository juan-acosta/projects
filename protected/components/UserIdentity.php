<?php

/**
 * UserIdentity represents the data needed to identity a user.
 *
 * @author Kalesis <juan.acosta@ddar.pe>
 * @version 4.0
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    const ERROR_EMAIL_INVALID = 3;
    const ERROR_STATUS_NOTACTIV = 4;
    const ERROR_STATUS_BAN = 5;
    const ERROR_EMAIL_PASSWD_INVALID = 6;

	/**
	 * Authenticates a user.
	 *
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{

        // row Usuario
        $user = Developer::model()->findByAttributes([
            'username' => $this->username
        ]);

        if( !$user ) // no existe el registro
        {
            // $this->errorCode = self::ERROR_EMAIL_INVALID;
            $this->errorCode = self::ERROR_EMAIL_PASSWD_INVALID; // toda falla es de usuario y clave
            return false;
        }

        if( Yii::app()->getModule('backend')->verify_hash( $this->password, $user->passwd) )
        {
            $this->errorCode = self::ERROR_EMAIL_PASSWD_INVALID; // toda falla es de usuario y clave
            return false;
        }

        if( $user->status_id === 0 )
        {
            $this->errorCode = self::ERROR_STATUS_NOTACTIV;
            return false;
        }

        if( $user->status_id === 2 )
        {
            $this->errorCode = self::ERROR_STATUS_BAN;
            return false;
        }

        // success
        $this->_id = $user->id;
        $this->errorCode = self::ERROR_NONE;

        return true;

	}

    /**
     * @return integer the ID of the user record
     */
    public function getId(){
        return $this->_id;
    }


}