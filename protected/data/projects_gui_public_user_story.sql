INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (5, '@juan.pillaca requiere de las sgtes mejoras:
1. En la sección de equipos instalados: IMC Nodo, IMC Cliente, Observación
2. En la sección Info Redes: Swithc Puerto (str), Switch: Passwd, User. IP Sector. Medio Fibra', 0, 1, '2018-12-26', '2018-12-26', 3, 0, 0, null, null, 3);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (9, 'Flujo de Prueba v2. Revisión de Errores.', 0, 1, '2019-01-03', '2018-12-10', 5, 0, 0, null, null, 4);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (10, 'Problema con API Facebook. 
Problema con Dominios gtdperu.pe', 0, 1, '2018-12-29', '2018-12-17', 6, 0, 0, null, null, 9);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (1, 'Leer con Python los XLS de factibilidades y guardar los campos Monto y Dias en Backlog (sdi_sede_servicio). 
OBS: Son dos archivos XLS diferentes.', 0, 1, '2018-12-24', '2018-12-10', 20, 0, 85, null, null, 6);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (3, 'Para el Directorio crear un nuevo campo "Objetivo Mes"', 0, 1, '2018-12-21', '2018-12-03', 6, 0, 100, null, null, 7);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (4, 'Pase a Producción: Objetivo Mes', 0, 1, '2018-12-21', '2018-12-03', 2, 0, 100, null, null, 7);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (2, '@luis.martinez tiene la sgtes lista de mejoras:
1. Reportes: Base de Evntos
2. Base de Contactos
3. Dashboard: update graph 01
4. Dashboard: new graph
5. Oportunidad: en Aceptacion
6. Aceptación: Fecha estimada de cierre
7. Producto: new form . (no viable)
8. Reporte: update Cliente
9. Factiblidad: Producto UM
10. View Report: Oportunidades por Cerrar
11. Eliminar Sedes
12. Cancelar Factibilidad
13. Texto: Perder y Reservar Oportunidad.', 0, 1, '2018-12-26', '2018-12-17', 48, 0, 63, null, null, 5);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (7, 'Módulo de Logs de Carga de Archivos', 0, 1, '2019-01-02', '2018-12-10', 12, 0, 75, null, null, 1);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (6, 'Módulo de Logs de Cambios', 0, 1, '2019-01-02', '2018-12-10', 12, 0, 75, null, null, 1);
INSERT INTO public.user_story (id, storie, points, developer_id, compromiso_date, inicio_date, hours_estimated, hours_effective, percent_finish, created_at, created_by, epic_id) VALUES (8, 'Módulo de Logs de Emails', 0, 1, '2019-01-02', '2018-12-10', 20, 0, 70, null, null, 1);