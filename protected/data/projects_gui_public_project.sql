INSERT INTO public.project (id, project, abstract, developer_id, https_git, created_at, created_by, client_id) VALUES (1, 'Plataforma WiFi', 'Site de Administración de Portales Cautivos, Dashboard, Usuarios/Clientes y Anuncios.
Creado el 2014', 1, 'https://kalesis@bitbucket.org/gtd-wigo/plataforma.git', '2018-12-26 05:30:33.980034', 1, 2);
INSERT INTO public.project (id, project, abstract, developer_id, https_git, created_at, created_by, client_id) VALUES (2, 'Portales Cautivos', 'Portal Cautivo de Anuncio y Codigos.
Creado el 2014', 1, 'https://kalesis@bitbucket.org/gtd-wigo/captive.git', '2018-12-26 05:39:57.669525', 1, 2);
INSERT INTO public.project (id, project, abstract, developer_id, https_git, created_at, created_by, client_id) VALUES (3, 'Intranet', 'Intranet CRM, Backlog, Security, etc
creado el 2017', 8, 'https://kalesis@bitbucket.org/gtd-wigo/intranet-v3.git', '2018-12-26 05:41:25.399246', 1, 2);