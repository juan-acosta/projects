create table task
(
	id serial not null
		constraint task_pk
			primary key,
	task text not null,
	developer_id integer not null
		constraint task_developer_id_fk
			references developer,
	hours_complete integer default 0,
	anio integer not null,
	semana integer not null,
	user_story_id integer
		constraint task_user_story_id_fk
			references user_story,
	created_at timestamp,
	created_by integer,
	sprint_us_id integer
		constraint task_sprint_us_id_fk
			references sprint_us
);

comment on table task is 'Tarea';

comment on column task.id is 'ID';

comment on column task.task is 'Tarea';

comment on column task.developer_id is 'Developer ID';

comment on column task.hours_complete is 'Horas trabajo';

comment on column task.anio is 'Anio';

comment on column task.semana is 'Semana';

comment on column task.user_story_id is 'Historia ID - FK';

comment on column task.created_at is 'Creado el';

comment on column task.created_by is 'Creado por';

comment on column task.sprint_us_id is 'Sprint User Story ID';

alter table task owner to postgres;

