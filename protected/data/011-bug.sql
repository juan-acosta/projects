create table bug
(
	id serial not null
		constraint bug_pk
			primary key,
	bug text not null,
	developer_id integer
		constraint bug_developer_id_fk
			references developer,
	created_at timestamp,
	created_by integer,
	hours_complete integer default 0,
	sprint_us_id integer
		constraint bug_sprint_us_id_fk
			references sprint_us
);

comment on table bug is 'Bugs';

comment on column bug.id is 'ID';

comment on column bug.bug is 'Bug';

comment on column bug.developer_id is 'Developer ID';

comment on column bug.created_at is 'Creado el';

comment on column bug.created_by is 'Creado por';

comment on column bug.hours_complete is 'Horas trabajo';

comment on column bug.sprint_us_id is 'Sprint User Story ID';

alter table bug owner to postgres;

