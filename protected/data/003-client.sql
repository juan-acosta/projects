create table client
(
	id serial not null
		constraint client_pk
			primary key,
	name_contributor varchar(250) not null,
	tradename varchar(250),
	ruc varchar(20) not null,
	address_tax_office text not null,
	actividad_id integer,
	created_at timestamp,
	created_by integer
);

comment on table client is 'Cliente Empresa';

comment on column client.id is 'ID';

comment on column client.name_contributor is 'Razon Social';

comment on column client.tradename is 'Nombre comercial';

comment on column client.ruc is 'RUC';

comment on column client.address_tax_office is 'Domicilio fiscal';

comment on column client.actividad_id is 'Actividad economica';

comment on column client.created_at is 'Creado el';

comment on column client.created_by is 'Creado por';

alter table client owner to postgres;

