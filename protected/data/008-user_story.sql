create table user_story
(
	id serial not null
		constraint user_story_pk
			primary key,
	storie text not null,
	points integer default 0,
	developer_id integer not null
		constraint user_story_developer_id_fk
			references developer,
	compromiso_date date not null,
	inicio_date date not null,
	hours_estimated integer default 0,
	hours_effective integer default 0,
	percent_finish integer default 0,
	created_at timestamp,
	created_by integer,
	epic_id integer
		constraint user_story_epic_id_fk
			references epic
);

comment on table user_story is 'Historia usuario';

comment on column user_story.id is 'ID';

comment on column user_story.storie is 'Historia Usuario';

comment on column user_story.points is 'Puntos de Historia';

comment on column user_story.developer_id is 'Developer ID';

comment on column user_story.compromiso_date is 'Fecha de compromiso';

comment on column user_story.inicio_date is 'Fecha de Inicio';

comment on column user_story.hours_estimated is 'Horas Estimadas';

comment on column user_story.hours_effective is 'Horas Efectivas';

comment on column user_story.percent_finish is 'Porcentaje Finalizar';

comment on column user_story.created_at is 'Creado el';

comment on column user_story.created_by is 'Creado por';

comment on column user_story.epic_id is 'Epic ID - FK';

alter table user_story owner to postgres;

