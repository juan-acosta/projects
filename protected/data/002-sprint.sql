create table sprint
(
	id serial not null
		constraint sprint_pk
			primary key,
	week_id varchar(7),
	created_at timestamp,
	created_by integer
);

comment on table sprint is 'Sprint Semanal';

comment on column sprint.id is 'ID';

comment on column sprint.week_id is 'Semana-Anio';

comment on column sprint.created_at is 'Creado el';

comment on column sprint.created_by is 'Creado por';

alter table sprint owner to homestead;

