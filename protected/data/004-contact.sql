create table contact
(
	id serial not null
		constraint contact_pk
			primary key,
	first_name varchar(128) not null,
	last_name varchar(128) not null,
	puesto_str varchar(32) not null,
	phone_oficina varchar(15) not null,
	phone_mobile varchar(15),
	created_at time,
	created_by integer,
	client_id integer
		constraint contact_client_id_fk
			references client
);

comment on table contact is 'Contactos Cliente';

comment on column contact.id is 'ID';

comment on column contact.first_name is 'Nombres';

comment on column contact.last_name is 'Apellidos';

comment on column contact.puesto_str is 'Puesto';

comment on column contact.phone_oficina is 'Telefono oficina';

comment on column contact.phone_mobile is 'Telefono celular';

comment on column contact.created_at is 'Creado el';

comment on column contact.created_by is 'Creado por';

comment on column contact.client_id is 'Cliente ID - FK';

alter table contact owner to postgres;

