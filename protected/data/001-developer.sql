create table developer
(
	id serial not null
		constraint developer_pk
			primary key,
	names varchar(250) not null,
	username varchar(25) not null,
	passwd varchar(250) not null,
	profile varchar(10) default 'admin'::character varying not null,
	email varchar(128) not null,
	created_at timestamp,
	created_by integer,
	status_id integer default 1 not null
);

comment on table developer is 'Developers';

comment on column developer.id is 'ID';

comment on column developer.names is 'Nombres completos';

comment on column developer.username is 'Usuario';

comment on column developer.passwd is 'Clave';

comment on column developer.profile is 'Rol';

comment on column developer.email is 'Email';

comment on column developer.created_at is 'Creado el';

comment on column developer.created_by is 'Creado por';

comment on column developer.status_id is 'Estado';

alter table developer owner to postgres;

