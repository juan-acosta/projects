create table epic
(
	id serial not null
		constraint epic_pk
			primary key,
	epic varchar(128),
	created_at timestamp,
	created_by integer,
	package_id integer
		constraint epic_package_id_fk
			references package
);

comment on table epic is 'Epic';

comment on column epic.id is 'ID';

comment on column epic.created_at is 'Creado el';

comment on column epic.created_by is 'Creado por';

comment on column epic.package_id is 'Package ID - FK';

alter table epic owner to postgres;

