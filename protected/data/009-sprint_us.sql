create table sprint_us
(
	id serial not null
		constraint sprint_us_pk
			primary key,
	sprint_id integer
		constraint sprint_us_sprint_id_fk
			references sprint,
	us_id integer
		constraint sprint_us_user_story_id_fk
			references user_story,
	cretaed_at timestamp,
	created_by integer
);

comment on table sprint_us is 'Sprint - User Story';

comment on column sprint_us.id is 'ID';

comment on column sprint_us.sprint_id is 'Sprint ID';

comment on column sprint_us.us_id is 'User Story ID';

comment on column sprint_us.cretaed_at is 'Creado el';

comment on column sprint_us.created_by is 'Creado por';

alter table sprint_us owner to homestead;

