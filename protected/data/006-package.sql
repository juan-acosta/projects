create table package
(
	id serial not null
		constraint package_pk
			primary key,
	package varchar(128),
	code varchar(16),
	created_at timestamp,
	created_by integer,
	project_id integer
		constraint package_project_id_fk
			references project
);

comment on table package is 'Paquete';

comment on column package.package is 'Paquete';

comment on column package.code is 'Alias';

comment on column package.created_at is 'Creado el';

comment on column package.created_by is 'Creado por';

comment on column package.project_id is 'Project ID - FK';

alter table package owner to postgres;

create unique index package_code_uindex
	on package (code);

