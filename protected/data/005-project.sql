create table project
(
	id serial not null
		constraint project_pk
			primary key,
	project varchar(200) not null,
	abstract text,
	developer_id integer not null
		constraint project_developer_id_fk
			references developer,
	https_git text,
	created_at timestamp,
	created_by integer,
	client_id integer
		constraint project_client_id_fk
			references client
);

comment on table project is 'Proyectos';

comment on column project.id is 'ID';

comment on column project.project is 'Nombre';

comment on column project.abstract is 'Resumen';

comment on column project.developer_id is 'Asignado a';

comment on column project.https_git is 'URL del Proyecto';

comment on column project.created_at is 'Creado el';

comment on column project.created_by is 'Creado por';

alter table project owner to postgres;

