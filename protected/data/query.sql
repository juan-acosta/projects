create database projects_gui
  with owner homestead;

create table client
(
  id                 serial       not null
    constraint client_pk
      primary key,
  name_contributor   varchar(250) not null,
  tradename          varchar(250),
  ruc                varchar(20)  not null,
  address_tax_office text         not null,
  actividad_id       integer,
  created_at         timestamp,
  created_by         integer
);

comment on table client is 'Cliente Empresa';

comment on column client.id is 'ID';

comment on column client.name_contributor is 'Razon Social';

comment on column client.tradename is 'Nombre comercial';

comment on column client.ruc is 'RUC';

comment on column client.address_tax_office is 'Domicilio fiscal';

comment on column client.actividad_id is 'Actividad economica';

comment on column client.created_at is 'Creado el';

comment on column client.created_by is 'Creado por';

alter table client
  owner to postgres;

create table contact
(
  id            serial       not null
    constraint contact_pk
      primary key,
  first_name    varchar(128) not null,
  last_name     varchar(128) not null,
  puesto_str    varchar(32)  not null,
  phone_oficina varchar(15)  not null,
  phone_mobile  varchar(15),
  created_at    time,
  created_by    integer,
  client_id     integer
    constraint contact_client_id_fk
      references client
);

comment on table contact is 'Contactos Cliente';

comment on column contact.id is 'ID';

comment on column contact.first_name is 'Nombres';

comment on column contact.last_name is 'Apellidos';

comment on column contact.puesto_str is 'Puesto';

comment on column contact.phone_oficina is 'Telefono oficina';

comment on column contact.phone_mobile is 'Telefono celular';

comment on column contact.created_at is 'Creado el';

comment on column contact.created_by is 'Creado por';

comment on column contact.client_id is 'Cliente ID - FK';

alter table contact
  owner to postgres;

create table developer
(
  id         serial                                         not null
    constraint developer_pk
      primary key,
  names      varchar(250)                                   not null,
  username   varchar(25)                                    not null,
  passwd     varchar(250)                                   not null,
  profile    varchar(10) default 'admin'::character varying not null,
  email      varchar(128)                                   not null,
  created_at timestamp,
  created_by integer,
  status_id  integer     default 1                          not null
);

comment on table developer is 'Developers';

comment on column developer.id is 'ID';

comment on column developer.names is 'Nombres completos';

comment on column developer.username is 'Usuario';

comment on column developer.passwd is 'Clave';

comment on column developer.profile is 'Rol';

comment on column developer.email is 'Email';

comment on column developer.created_at is 'Creado el';

comment on column developer.created_by is 'Creado por';

comment on column developer.status_id is 'Estado';

alter table developer
  owner to postgres;

create table project
(
  id           serial       not null
    constraint project_pk
      primary key,
  project      varchar(200) not null,
  abstract     text,
  developer_id integer      not null
    constraint project_developer_id_fk
      references developer,
  https_git    text,
  created_at   timestamp,
  created_by   integer,
  client_id    integer
    constraint project_client_id_fk
      references client
);

comment on table project is 'Proyectos';

comment on column project.id is 'ID';

comment on column project.project is 'Nombre';

comment on column project.abstract is 'Resumen';

comment on column project.developer_id is 'Asignado a';

comment on column project.https_git is 'URL del Proyecto';

comment on column project.created_at is 'Creado el';

comment on column project.created_by is 'Creado por';

alter table project
  owner to postgres;

create table package
(
  id         serial not null
    constraint package_pk
      primary key,
  package    varchar(128),
  code       varchar(16),
  created_at timestamp,
  created_by integer,
  project_id integer
    constraint package_project_id_fk
      references project
);

comment on table package is 'Paquete';

comment on column package.package is 'Paquete';

comment on column package.code is 'Alias';

comment on column package.created_at is 'Creado el';

comment on column package.created_by is 'Creado por';

comment on column package.project_id is 'Project ID - FK';

alter table package
  owner to postgres;

create unique index package_code_uindex
  on package (code);

create table epic
(
  id         serial not null
    constraint epic_pk
      primary key,
  epic       varchar(128),
  created_at timestamp,
  created_by integer,
  package_id integer
    constraint epic_package_id_fk
      references package
);

comment on table epic is 'Epic';

comment on column epic.id is 'ID';

comment on column epic.created_at is 'Creado el';

comment on column epic.created_by is 'Creado por';

comment on column epic.package_id is 'Package ID - FK';

alter table epic
  owner to postgres;

create table user_story
(
  id              serial  not null
    constraint user_story_pk
      primary key,
  storie          text    not null,
  points          integer default 0,
  developer_id    integer not null
    constraint user_story_developer_id_fk
      references developer,
  compromiso_date date    not null,
  inicio_date     date    not null,
  hours_estimated integer default 0,
  hours_effective integer default 0,
  percent_finish  integer default 0,
  created_at      timestamp,
  created_by      integer,
  epic_id         integer
    constraint user_story_epic_id_fk
      references epic
);

comment on table user_story is 'Historia usuario';

comment on column user_story.id is 'ID';

comment on column user_story.storie is 'Historia Usuario';

comment on column user_story.points is 'Puntos de Historia';

comment on column user_story.developer_id is 'Developer ID';

comment on column user_story.compromiso_date is 'Fecha de compromiso';

comment on column user_story.inicio_date is 'Fecha de Inicio';

comment on column user_story.hours_estimated is 'Horas Estimadas';

comment on column user_story.hours_effective is 'Horas Efectivas';

comment on column user_story.percent_finish is 'Porcentaje Finalizar';

comment on column user_story.created_at is 'Creado el';

comment on column user_story.created_by is 'Creado por';

comment on column user_story.epic_id is 'Epic ID - FK';

alter table user_story
  owner to postgres;

create table sprint
(
  id         serial not null
    constraint sprint_pk
      primary key,
  week_id    varchar(7),
  created_at timestamp,
  created_by integer
);

comment on table sprint is 'Sprint Semanal';

comment on column sprint.id is 'ID';

comment on column sprint.week_id is 'Semana-Anio';

comment on column sprint.created_at is 'Creado el';

comment on column sprint.created_by is 'Creado por';

alter table sprint
  owner to homestead;

create table sprint_us
(
  id         serial not null
    constraint sprint_us_pk
      primary key,
  sprint_id  integer
    constraint sprint_us_sprint_id_fk
      references sprint,
  us_id      integer
    constraint sprint_us_user_story_id_fk
      references user_story,
  cretaed_at timestamp,
  created_by integer
);

comment on table sprint_us is 'Sprint - User Story';

comment on column sprint_us.id is 'ID';

comment on column sprint_us.sprint_id is 'Sprint ID';

comment on column sprint_us.us_id is 'User Story ID';

comment on column sprint_us.cretaed_at is 'Creado el';

comment on column sprint_us.created_by is 'Creado por';

alter table sprint_us
  owner to homestead;

create table task
(
  id             serial  not null
    constraint task_pk
      primary key,
  task           text    not null,
  developer_id   integer not null
    constraint task_developer_id_fk
      references developer,
  hours_complete integer default 0,
  anio           integer not null,
  semana         integer not null,
  user_story_id  integer
    constraint task_user_story_id_fk
      references user_story,
  created_at     timestamp,
  created_by     integer,
  sprint_us_id   integer
    constraint task_sprint_us_id_fk
      references sprint_us
);

comment on table task is 'Tarea';

comment on column task.id is 'ID';

comment on column task.task is 'Tarea';

comment on column task.developer_id is 'Developer ID';

comment on column task.hours_complete is 'Horas trabajo';

comment on column task.anio is 'Anio';

comment on column task.semana is 'Semana';

comment on column task.user_story_id is 'Historia ID - FK';

comment on column task.created_at is 'Creado el';

comment on column task.created_by is 'Creado por';

comment on column task.sprint_us_id is 'Sprint User Story ID';

alter table task
  owner to postgres;

create table bug
(
  id             serial not null
    constraint bug_pk
      primary key,
  bug            text   not null,
  developer_id   integer
    constraint bug_developer_id_fk
      references developer,
  created_at     timestamp,
  created_by     integer,
  hours_complete integer default 0,
  sprint_us_id   integer
    constraint bug_sprint_us_id_fk
      references sprint_us
);

comment on table bug is 'Bugs';

comment on column bug.id is 'ID';

comment on column bug.bug is 'Bug';

comment on column bug.developer_id is 'Developer ID';

comment on column bug.created_at is 'Creado el';

comment on column bug.created_by is 'Creado por';

comment on column bug.hours_complete is 'Horas trabajo';

comment on column bug.sprint_us_id is 'Sprint User Story ID';

alter table bug
  owner to postgres;

