<?= "<?php\n"; ?>

/**
 * Description for <?= $this->moduleClass."\n"; ?>
 *
 * @author Kalesis <juan.acosta@ddar.pe><?= "\n"; ?>
 * @version 4.0<?= "\n"; ?>
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class <?= $this->moduleClass; ?> extends CWebModule
{
    public function init()
    {
        // import the module-level models and components
        $this->setImport([
            '<?= $this->moduleID; ?>.models.*',
            '<?= $this->moduleID; ?>.components.*',
        ]);
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }
}
