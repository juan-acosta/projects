<?= "<?php\n"; ?>

/**
 * Description for DefaultController
 *
 * @var $this DefaultController<?= "\n"; ?>
 * @package <?= $this->moduleClass."\n"; ?>
 * @author Kalesis <juan.acosta@ddar.pe><?= "\n"; ?>
 * @version 4.0<?= "\n"; ?>
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }
}