<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?= "<?php\n"; ?>

/**
 * Model class for table <?= $tableName; ?>.
 *
 * The columns in table:
<?php foreach($columns as $column): ?>
 * @property <?= $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * The model relations:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
	if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = $matches[2];

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
	}
    ?>
<?php endforeach; ?>
<?php endif; ?>
 *
 * @author Kalesis <juan.acosta@ddar.pe><?= "\n"; ?>
 * @version 4.0<?= "\n"; ?>
 * @copyright 2019 Development & Data Analysis & Reports EIRL
 */
class <?= $modelClass; ?> extends <?= $this->baseClass."\n"; ?>
{
    // add field id_crypt
    public $id_crypt;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '<?= $tableName; ?>';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
<?php foreach($rules as $rule): ?>
<?php $_tmp_rule = str_replace(['array(',')'] ,['[',']'] ,$rule) ?>
            <?= $_tmp_rule.",\n"; ?>
<?php endforeach; ?>
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['<?= implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'],

            // @todo Please remove those attributes that should not be searched.
            //['created_at', 'default', 'value'=> new CDbExpression('NOW()'), 'setOnEmpty'=> false, 'on'=>'insert' ],
            //['created_by', 'default', 'value'=> Yii::app()->user->id, 'setOnEmpty'=> false, 'on'=>'insert' ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
<?php foreach($relations as $name=>$relation): ?>
<?php $_tmp_relation = str_replace(['array(',')'] ,['[',']'] ,$relation) ?>
            <?= "'$name' => $_tmp_relation,\n"; ?>
<?php endforeach; ?>
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
<?php foreach($labels as $name=>$label): ?>
            <?= "'".$name."' => '".str_replace("'","\'",$label)."',\n"; ?>
<?php endforeach; ?>
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria=new CDbCriteria;

        // @todo Please modify the following code to remove attributes that should not be searched.
<?php
foreach($columns as $name=>$column)
{
    if($column->type==='string')
    {
        echo "        \$criteria->compare('$name',\$this->$name,true);\n";
    }
    else
    {
        echo "        \$criteria->compare('$name',\$this->$name);\n";
    }
}
?>

        return new CActiveDataProvider($this, ['criteria'=>$criteria,]);
    }

<?php if($connectionId!='db'):?>
    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()-><?= $connectionId ?>;
    }

<?php endif?>
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return <?= $modelClass; ?> the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     *
     */
    public function afterFind()
    {
        $crypt = new MyCrypt;
        $crypt->setIv(Yii::app()->params['my_iv']);
        $crypt->setPasswd(Yii::app()->params['my_passwd']);

        // Encrypt ID
        $this->id_crypt = $crypt->encrypt($this->id);

        return parent::afterFind();
    }

}
