<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
$label=$this->pluralize($this->class2name($this->modelClass));
?>
<?= "<?php\n"; ?>
/** @var $this <?=$this->getControllerClass(); ?> */
/** @see <?= $this->controller; ?>/create [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = '<?= $label;?>';
$this->headTitle = 'Crear <?= $this->class2name($this->modelClass);?>';

<?php
echo "\$this->breadcrumbs = [
    '$label' => ['index'],
    'Crear',
];\n";
?>
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Completar el formulario:</h4>
        <?= "<?= \$this->renderPartial('_form', ['model'=>\$model] ); ?>\n"; ?>
    </div>
</div>