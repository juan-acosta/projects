<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?= "<?php\n"; ?>
/** @var $this <?=$this->getControllerClass(); ?> */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */
?>

<?= "<?php\n"; ?>
<?php echo "\$form=\$this->beginWidget('CActiveForm', [
    'id'=>'".$this->class2id($this->modelClass)."-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'class' => 'ui fluid form'
    ],
    //'focus'=>[\$model,'<one_filed_primary>'],
]); ?>\n"; ?>

    <?= "<?php //echo \$form->errorSummary(\$model, '',''); ?>\n"; ?>

<?php
foreach($this->tableSchema->columns as $column)
{
    //var_dump($column);
    if( $column->autoIncrement || in_array($column->name,['created_at', 'created_by','updated_at', 'updated_by']) )
        continue;
?>
    <div class="fields <?= '<?=';?> $model->hasErrors('<?= $column->name;?>')?'error':'';?>" >
        <div class="three wide field " style="text-align: right;">
            <?= "<?= \$form->labelEx(\$model, '{$column->name}', ['class'=>'']); ?>\n"; ?>
        </div>
        <div class="thirteen wide field">
        <?php //echo $column->type;
        if($column->type==='boolean') {
            echo "    <?= \$form->checkBox(\$model,'{$column->name}'); ?>\n" ;
        } elseif ($column->type==='text') {
            echo "    <?= \$form->textArea(\$model,'{$column->name}',['class'=>'','rows'=>'3']); ?>\n";
        } else {
            if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
                $inputField='passwordField';
            else
                $inputField='textField';
            echo "    <?= \$form->{$inputField}(\$model,'{$column->name}',['class'=>'', ]); ?>\n";
        }
        ?>
            <?= "<?= \$form->error(\$model,'{$column->name}',['class'=>'ui pointing red basic label', ]); ?>\n"; ?>
        </div>
    </div>

<?php } ?>
    <br/>
    <div class="fields">
        <div class="three wide field"></div>
        <div class="thirteen wide field">
            <button class="ui primary button" type="submit" name="action">
                <i class="save icon"></i> <?= "<?= \$model->isNewRecord ? 'Grabar' : 'Actualizar';?>\n"; ?>
            </button>
            <?= "<?= CHtml::link('Cancelar', ['/$this->controller'], ['class'=>'mini ui button']); ?>\n"; ?>
        </div>
    </div>

<?= "<?php \$this->endWidget(); ?>\n"; ?>
