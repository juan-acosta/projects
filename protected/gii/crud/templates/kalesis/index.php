<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
$label = $this->pluralize($this->class2name($this->modelClass));
?>
<?= "<?php\n"; ?>
/** @var $this <?=$this->getControllerClass(); ?> */
/** @see <?= $this->controller; ?>/index [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$baseUrl = Yii::app()->baseUrl;
$js = Yii::app()->getClientScript();
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.ba-bbq.js',CClientScript::POS_END);
$js->registerScriptFile($baseUrl.'/js/yii/yii.jquery.yiigridview.js',CClientScript::POS_END);

$this->pagetitle = '<?= $label;?>';
$this->headTitle = 'Administrar <?= $label;?>';

$this->btnActions[] = [
    'name' => '<em class="fa fa-plus"></em> Crear',
    'url' => ['/<?= $this->controller; ?>/create'],
    'color' => 'primary',
];

<?php
echo "\$this->breadcrumbs = [
    '$label' => ['index'],
    'Administrar',
];\n";
?>

$dataProvider = $model->search();
$dataProvider->pagination = ['pageSize'=>15];
?>

<div class="row">
    <div class="col-md-12">

    <?= "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', [
        'id'=>'<?= $this->class2id($this->modelClass); ?>-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        //'rowCssClassExpression' => '$data->active == FALSE ? "danger":NULL',
        'columns' => [
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
    //var_dump($column);
    if(++$count==5)
        echo "            /*\n";
    if($count>=5){
        echo "            '".$column->name."',\n";
    } else {
        echo "            [\n";
        echo "                //'class'=>'DataColumn',\n";
        echo "                'name'=>'".$column->name."',\n";
        echo "                //'header'=>'".$column->name."',\n";
        echo "                //'value'=>'".$column->name."',\n";
        echo "                'type'=>'raw',\n";
        echo "                //'filter'=>false,\n";
        echo "                'htmlOptions'=>['class'=>''],\n";
        echo "            ],\n";
    }

}
if($count>=5)
	echo "            */\n";
?>
            [
                'class'=>'CButtonColumn',
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => [
                        'label' => '<i class="trash alternate outline icon"></i>',
                        'options' => ['title'=>'Eliminar', 'class'=>'negative ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/<?= $this->controller; ?>/delete/",["<?= $this->tableSchema->primaryKey; ?>"=>$data->id_crypt])'
                    ],
                    'update' => [
                        'label' => '<i class="edit outline icon"></i>',
                        'options' => ['title'=>'Actualizar', 'class'=>'teal ui compact icon button'],
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/<?= $this->controller; ?>/update/",["<?= $this->tableSchema->primaryKey; ?>"=>$data->id_crypt])'
                    ]
                ],
                //'visible' => Yii::app()->user->checkAccess([User::ROL_ADMIN]),
                'htmlOptions' => ['style'=>'width:110px','class'=>'center-align valign']
            ],
        ],
    ]); ?>

    </div>
</div>
