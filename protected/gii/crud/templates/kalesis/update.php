<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
?>
<?= "<?php\n"; ?>
/** @var $this <?=$this->getControllerClass(); ?> */
/** @see <?= $this->controller; ?>/update [controller/method] */
/** @author Kalesis <juan.acosta@ddar.pe> */
/** @version 4.0 */
/** @copyright 2019 Development & Data Analysis & Reports EIRL */

$this->pagetitle = '<?= $label;?>';
$this->headTitle = 'Actualizar el/la <?= $this->modelClass;?> ID#'.<?= "\$model->{$this->tableSchema->primaryKey}";?>;

<?php
echo "\$this->breadcrumbs = [
    '$label' => ['index'],
    'Actualizar',
];\n";
?>
?>

<div class="panel">
    <div class="panel-body">
        <h4 class="">Actualizar los datos del formulario:</h4>
        <?= "<?= \$this->renderPartial('_form', ['model'=>\$model]); ?>\n"; ?>
    </div>
</div>